Rails.application.routes.draw do
  
  resources :users
  resources :microposts
   
  # get   "/users", to: "users#index"
  # get   "/users/:id", to: "users#show"
  # get   "/users/:new", to: "users#new"
  # post  "users", to: "users#create"
  # put   "users/:id", to: "users#edit"
  # patch "users/:id", to: "users#update"
  # delete  "users/:id", to: "users#destroy"
end
  
               
