class User < ApplicationRecord
  validates :name, presence: true
  # validates :email, format: { with: /\A[a-zA-Z]+\z/}
  validate :email
  has_many :microposts
end
