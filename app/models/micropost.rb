class Micropost < ApplicationRecord
  validates :content, presence: true
  validates :content, uniqueness: true, presence: true
  belongs_to :user
end
